package me.Azurethi.logManager;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class Main {

	private static ArrayList<Player> players = new ArrayList<Player>();
	private static ArrayList<IP> ips = new ArrayList<IP>();
	public static String datadir = null;
	public static GUI ui = new GUI();
	public static SimpleDateFormat format = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");

	
	public static void main(String[] args){
		ui.println("Welcome. Use the \"help\" command to get started!");
		
		//TEST-- load 														-PASS
		//TEST-- parse 														-PASS
		//TEST-- save   													-PASS
		//TEST-- reload -error IP uuid lists not reloaded correctly.		-FAIL
		//					-Incorrect substring in IP.tostring(), fixed;	-fix
		//TEST-- load,parse, display, save, reset, reload, display 			-PASS
		
		//--Starting UI implementation.
		
		
		/*
		ArrayList<String> metrics = loadNewMetrics("F:/other/CPS/Backup Project/Test data/2017-02-26.met");
		parseNewMetrics(metrics);
		saveParsedMetrics("F:/other/CPS/Backup Project/Test data/attempt2.pmet");
		debugDisplay();
		//reset arrays
		players = new ArrayList<Player>();
		ips = new ArrayList<IP>();
		//load
		loadParsedMetrics("F:/other/CPS/Backup Project/Test data/attempt2.pmet");
		debugDisplay();
		*/
	}
	
	private static void debugDisplay(){
		String dump = "Players:\n";
		
		for(Player s : players){
			dump += "  "+s.toString()+"\n";
		}
		dump += "IPs:\n";
		for(IP ip : ips){
			dump += "  "+ip.toString()+"\n";
		}
		ui.println(dump, Color.GRAY);
	}
	
	private static void saveParsedMetrics(String path) {
		
		File pmet = new File(path);
		path = pmet.getAbsolutePath();
		if(pmet.exists()) pmet.delete();
		
		String data = "$PARSEDMETRICS&&";
		for(Player p : players){
			data += p.toString() + "&";
		}
		data = data.substring(0, data.length() - 2) + "&&";
		for(IP ip : ips){
			data += ip.toString() + "&";
		}
		data = data.substring(0, data.length() - 2);
		try{
			ui.println(path);
			PrintWriter out = new PrintWriter(path);
			out.println(data);
			out.close();
			ui.println("Completed.",Color.GREEN);
		}catch(Exception e){
			ui.println("Failed to save to file: " +e.getMessage(), Color.RED);
		}
		
	}
	
	private static void loadParsedMetrics(String path){
		File pmetdata = new File(path);
		try{
			BufferedReader in = new BufferedReader(new FileReader(pmetdata));
			String line = in.readLine();
			in.close();
			if(line == null) throw new Exception("null pmet data");
			if(!line.startsWith("$PARSEDMETRICS")) throw new Exception("malformed pmet data");
			String[] parsedData = line.split("&&");
			for(String playerString : parsedData[1].split("&")){
				players.add(Player.fromString(playerString));
			}
			for(String ipString :parsedData[2].split("&")){
				ips.add(IP.fromString(ipString));
			}
		}catch(Exception e){
			ui.println("Failed to load parsed metrics: " +e.getMessage(), Color.RED);
		}
		ui.println("Completed.",Color.GREEN);
		arrageTable(0,"");
	}

	private static ArrayList<String> loadNewMetrics(String path){
		ArrayList<String> metrics = new ArrayList<String>();
		File metFile = new File(path);
		if(metFile.isDirectory()){
			for(String f : metFile.list()){
				if(f.endsWith(".met")) metrics.addAll(loadNewMetrics(path +"\\" + f));
			}
			return metrics;
		}
		if(!path.endsWith(".met")) return null;
		ui.println("Loading from \"" + path +"\"...", Color.ORANGE);
		try{
			BufferedReader metIn = new BufferedReader(new FileReader(metFile));
			String line = "";
			while((line = metIn.readLine()) != null){
				if(line.startsWith("[")) metrics.add(metFile.getName().substring(0,metFile.getName().length() - 4) + " " +line);
			}
			metIn.close();
		}catch(Exception e){
			ui.println("Failed to load data from file: " +e.getMessage(), Color.RED);
			return null;
		}
		return metrics;
	}
	
	private static void parseNewMetrics(ArrayList<String> metrics){
		try {
		SimpleDateFormat sdf  = new SimpleDateFormat("yyyy-MM-dd [HH-mm-ss");
		for(String event : metrics){
			if(event.contains(":") && event.split(":").length >= 2){
				String[] eventData = event.replace("]", "").split(":");
				Long time  = sdf.parse(eventData[0]).getTime();
				switch(eventData[1]){
				case "PING":
					getip(eventData[2]).addPing(time);
					break;
				case "JOIN":
					getip(eventData[2]).addUUID(eventData[3]);
					Player p = getplayer(eventData[3]);
					p.addName(eventData[4]);
					p.startSession(eventData[2], time);
					break;
				case "QUIT":
					getip(eventData[2]).addUUID(eventData[3]);
					Player pl = getplayer(eventData[3]);
					pl.addName(eventData[4]);
					pl.endSession(time);
					break;
				default:
					ui.println("Unknown event type:" + event.split(":")[1]);
				}
				
			}else{
				ui.println("Malformed event string :" + event);
			}
			
		}
		ui.println("Completed.",Color.GREEN);
		} catch (Exception e) {
			ui.println("Failed to parse metrics: " +e.getMessage(), Color.RED);
		}
		arrageTable(0,"");
	}

	private static IP getip(String addr){
		for(IP ip : ips){
			if(ip.address.equals(addr)) return ip;
		}
		IP newip = new IP(addr);
		ips.add(newip);
		return newip;
	}
	
	private static Player getplayer(String uuid){
		for(Player p : players){
			if(p.uuid.equals(uuid)) return p;
		}
		Player newplayer = new Player(uuid);
		players.add(newplayer);
		return newplayer;
	}
	
	public static void parseCommand(String command){
		ui.println("> " + command);
		String[] args = command.split(" ");
		if(args.length > 0) switch(args[0].toLowerCase()){
			case "help":
				ui.println(	"Commands: \n" +
							"  help\n" +
							"     -Display's this message\n"+
							"  dump\n"+
							"     -writes all of the loaded data as text to console\n"+
							"  parse <path>\n"+
							"     -parse new metrics from a (.met) file into the loaded metrics\n"+
							"  load <path>\n"+
							"     -load parsed metrics file (.pmet)\n"+
							"  save <path>\n"+
							"     -save all loaded metrics to a file (.pmet)\n"+
							"  sort <Column name>\n"+
							"     -sorts the records displayed by the given column\n"+
							"  search <type> <criteria>\n"+
							"     -Types:\n"+
							"            -Name\n"+
							"            -UUID\n"+
							"            -IP\n"+
							"     -Search for one of the above that matches the given criteria\n"+
							"  expand <type> <id>\n"+
							"     -Types:\n"+
							"            -player <UUID>\n"+
							"            -ip <address>\n"+
							"     -Show all info on the given player or ip\n"+
							"  collapse\n"+
							"     -return to default list view\n"+
							"  flag\n"+
							"     -displays a list of ip's with high ping : join ratios\n"
							);
				return;
			case "dump":
				ui.println("Starting dump...",Color.ORANGE);
				debugDisplay();
				ui.println("Completed.",Color.GREEN);
				return;
			case "parse":
				if(args.length < 2){
					ui.println("Please Specify a path", Color.RED);
				}else{
					String path = command.toLowerCase().replaceFirst("parse ","");
					ui.println("Parsing from \"" + path +"\"...", Color.ORANGE);
					ArrayList<String> metrics = loadNewMetrics(path);
					parseNewMetrics(metrics);
				}
				return;
			case "load":
				if(args.length < 2){
					ui.println("Please Specify a path", Color.RED);
				}else{
					String path = command.toLowerCase().replaceFirst("load ","");
					
					loadParsedMetrics(path);
				}
				return;
			case "save":
				if(args.length < 2){
					ui.println("Please Specify a path", Color.RED);
				}else{
					String path = command.toLowerCase().replaceFirst("save ","");
					ui.println("Saving to \"" + path +"\"...", Color.ORANGE);
					saveParsedMetrics(path);
				}
				return; 
			case "sort":
				if(args.length < 2){
					ui.println("Please Specify a column name", Color.RED);
				}else{
					arrageTable(0,"~sort:" + command.toLowerCase().replaceFirst("sort ",""));
				}
				return;
			case "search":
				if(args.length < 3){
					ui.println("Please Specify a type and criteria", Color.RED);
				}else{
					switch(args[1].toLowerCase()){
					case"name":
						HashSet<String> found = new HashSet<String>();
						ui.println("Searching for playernames containing \"" + args[2] + "\"");
						for(Player p : players){
							for(String name : p.names){
								if(name.toLowerCase().contains(args[2]) && !found.contains(p.uuid)){
									found.add(p.uuid);
									ui.println("  " + p.uuid);
									for(String cname : p.names){
										ui.println("    -" + cname, (name.equals(cname) ? Color.GREEN : Color.GRAY));
									}
								}
							}
						}
						ui.println("Completed!", Color.GREEN);
						return;
					case"uuid":
						HashSet<String> foundUUIDS = new HashSet<String>();
						ui.println("Searching for UUID's containing \"" + args[2] + "\"");
						for(Player p : players){
							if(p.uuid.toLowerCase().contains(args[2]) && !foundUUIDS.contains(p.uuid)){
								foundUUIDS.add(p.uuid);
								ui.println("  " + p.uuid, Color.green);
								for(String cname : p.names){
									ui.println("    -" + cname, Color.GRAY);
								}
							}
						}
						return;
					case"ip":
						HashSet<String> foundIPS = new HashSet<String>();
						ui.println("Searching for UUID's containing \"" + args[2] + "\"");
						for(IP ip : ips){
							if(ip.address.toLowerCase().contains(args[2]) && !foundIPS.contains(ip.address)){
								foundIPS.add(ip.address);
								ui.println("  " + ip.address, Color.green);
								for(String uuid : ip.uuids){
									Player p = getplayer(uuid);
									ui.println("    +" + uuid);
									for(String cname : p.names){
										ui.println("        -" + cname, Color.GRAY);
									}
								}
							}
						}
						return;
					default :
						ui.println("unknown Type, see help", Color.RED);
						return;
					}
				}
				return;
			case "collapse":
				arrageTable(0,"");
				return;
			case "expand":
				if(args.length <= 2){
					ui.println("Please Specify a type and id", Color.RED);
				}else{
					int mode = 0;
					String params = null;
					switch(args[1].toLowerCase()){
					case"player":
						for(Player p : players){
							if(p.uuid.contains(args[2])){
								params = "~player:"+p.uuid;
								break;
							}
						}
						if(params == null){
							ui.println("Could not find a player with that uuid", Color.RED);
							return;
						}
						mode = 1;
						break;
					case"ip":
						for(IP ip : ips){
							if(ip.address.contains(args[2])){
								params = "~ip:"+ip.address;
							}
						}
						if(params == null){
							ui.println("Could not find any matching ips", Color.RED);
							return;
						}
						mode = 2;
						break;
					default:
						ui.println("unknown Type, see help", Color.RED);
						return;
					}
					arrageTable(mode,params);
				}
				return;
			case "flag":
				for(IP ip : ips){
					int joins = 0;
					for(String uuid : ip.uuids){
						Player p = getplayer(uuid);
						for(Session s : p.sessions){
							if(s.ip.equals(ip.address)) joins++;
						}
					}
					double ratio = (double)joins/ip.pings.size();
					Color c = Color.GREEN;
					if(ratio < 0.2) c = Color.orange;
					if(ratio < 0.01) c = Color.red;
					ui.println(ip.address +" "+ joins + " joins, "+ ip.pings.size() + " pings (" + ratio + ")",c);
				}
				return;
			/*case "test":
				parseCommand("parse A:\\2017-02-26.met");
				parseCommand("dump");
				return;*/
		}
		
		ui.println("Unknown command...", Color.RED);
	}
	
	private static void arrageTable(int mode, String param){
		String[] head = null, data[] = null;
		switch(mode){
		case 0:
			head = new String[]{"Last Seen Name", "Last seen", "ip", "uuid", "#joins", "#pings","#ips","#names"};
			data = new String[players.size()][8];
			int pos = 0;
			for(Player p : players){
				
				data[pos][0] = p.names.get(p.names.size() - 1);
				Session lastSession;
				try{
					lastSession = p.sessions.get(p.sessions.size()-1);
				}catch(Exception e){
					lastSession = new Session("Unknown",0);
					lastSession.end(0);
				}
				if(lastSession.getEnd() != -1){
					data[pos][1] = format.format(new Date(lastSession.getEnd()));
				}else{
					data[pos][1] = "*" +  format.format(new Date(lastSession.getStart()));
				}
				data[pos][2] = lastSession.ip;
				data[pos][3] = p.uuid;
				data[pos][4] = ""+p.sessions.size();
				data[pos][5] = ""+getip(lastSession.ip).pings.size();
				HashSet<String> uniqueIps = new HashSet<String>();
				for(Session s : p.sessions) uniqueIps.add(s.ip);
				data[pos][6] = ""+uniqueIps.size();
				data[pos][7] = ""+p.names.size();
				
				pos++;
			}
			break;
		case 1:
			String uuid = null;
			try{
				uuid = param.split("~player:")[1].split("~")[0];
			}catch(Exception e){
				uuid = null;
			}
			if(uuid == null){
				ui.println("Arrange table mode error 0x01 : invalid uuid", Color.red);
				return;
			}
			Player p = getplayer(uuid);
			HashSet<String> ips = new HashSet<String>();
			for(Session s : p.sessions) ips.add(s.ip);
			head = new String[]{"ip", "#sessions", "usual login time", "avg play time","total play time", "#pings","#others"};
			data = new String[ips.size()][7];
			int pos2 = 0;
			HashSet<String> RelatedUUIDs = new HashSet<String>();
			for(String addr : ips){
				 ArrayList<Session> tempSessionStore = new ArrayList<Session>();
				 for(Session s : p.sessions){
					 if(s.ip.equals(addr)) tempSessionStore.add(s);
				 }
				 
				 data[pos2][0] = addr;
				 data[pos2][1] = ""+tempSessionStore.size();
				 long loginTime = 0, playtime = 0;
				 int offset = 0;
				 SimpleDateFormat timeonly = new SimpleDateFormat("HH:mm:ss");
				 for(Session s : tempSessionStore){
					 try{
						 loginTime += timeonly.parse(timeonly.format(new Date(s.getStart()))).getTime();
						 if(s.getEnd() != -1) playtime += s.getLength();
					 }catch(Exception e){
						 offset++;
					 }
				 }
				 loginTime = loginTime/(tempSessionStore.size() - offset);
				 data[pos2][2] =""+timeonly.format(new Date(loginTime));
				 data[pos2][3] =  playtime/((tempSessionStore.size() - offset) * 60000)+"min";
				 data[pos2][4] = playtime/60000+"min";
				 IP cIP = getip(addr);
				 data[pos2][5] = ""+cIP.pings.size();
				 data[pos2][6] = ""+(cIP.uuids.size()-1);
				 
				 for(String reluuid: cIP.uuids) RelatedUUIDs.add(reluuid);
				 
				 pos2++;
				 
			}
			ui.println("Related UUIDs:");
			for(String uuidz : RelatedUUIDs){
				if(!uuidz.equals(uuid)) ui.println(uuidz);
			}
			break;
		}
		if(param.contains("~sort:")){
			int ColIndex = head.length;
			String val = param.split("~sort:")[1].split("~")[0];
			try{
				ColIndex = Integer.parseInt(val);
			}catch(Exception ignored){}
			if(ColIndex >= head.length){
				ui.println("Searching for colum \"" + val + "\"",Color.orange);	//XXX Linear Search
				int i;
				for(i=0;i<head.length;i++){
					if(head[i].equalsIgnoreCase(val)){
						ColIndex = i;
					}
				}
			}

			if(ColIndex < head.length){
				for(int r = 1; r < data.length; r++){	//XXX Insersion sort
				    	   int pos = r;							
				           String[] tempRow = data[pos];
				           while(pos > 0 && (compare(tempRow[ColIndex],data[pos-1][ColIndex]) )){
				               data[pos] = data[pos-1];	
				               pos--;
				           }
				           data[pos] = tempRow;
				       }
			}else{
				ui.println("Could not find colum!", Color.red);
			}
			
		}
		ui.updateTable(head, data);
		
	}

	private static boolean compare(String a, String b) {
		long ia,ib;
		
		try{
			ia = Integer.parseInt(a);
			ib = Integer.parseInt(b);
			if(ia > ib) return true;
			return false;
		}catch(Exception e){}
		
		try{
			 ia = format.parse(a.charAt(0) == '*' ? a.substring(1):a).getTime();
			 ib = format.parse(b.charAt(0) == '*' ? b.substring(1):b).getTime();
			 if(ia > ib) return true;
			 return false;
		}catch(Exception e){}
		
		return a.compareTo(b) < 0;
	}


}
