package me.Azurethi.logManager;

import java.util.ArrayList;

public class Player {
	
	public ArrayList<String> names = new ArrayList<String>();
	public String uuid = "";
	public ArrayList<Session> sessions = new ArrayList<Session>();
	private Session tempSession;

	
	public Player(String UUID){
		uuid = UUID;
	}
	
	public void startSession(String ip, long time){
		if(tempSession != null){
			endSession(time);
		}
		tempSession = new Session(ip, time);
	}
	
	public void endSession(long time){
		if(tempSession != null){
			tempSession.end(time);
			sessions.add(tempSession);
			tempSession = null;
		}
	}
	
	public void addName(String name){
		if(!names.contains(name)) names.add(name);
	}
	
	public String toString(){
		String ret = "[player#" + uuid +"#";
		for(String name : names){
			ret += name + ",";
		}
		ret = ret.substring(0, ret.length() - 1) +"#";
		if(tempSession != null) endSession(-1);
		for(Session session : sessions){
			ret += session.toString() + "~";
		}
		return ret.substring(0, ret.length() - 1) +"]";
	}
	
	public static Player fromString(String data){
		if(!data.startsWith("[player#")){
			return null;
		}
		String[] vals = data.split("#");
		Player p = new Player(vals[1]);
		for(String name : vals[2].split(",")){
			p.addName(name);
		}
		for(String sessionString : vals[3].split("~")){
			Session s = Session.fromString(sessionString);
			if(s.getEnd() != -1){
				p.sessions.add(s);
			}else{
				p.tempSession = s;
			}
			
		}
		return p;
	}
	
}
