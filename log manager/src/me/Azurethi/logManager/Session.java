package me.Azurethi.logManager;

public class Session {

	private long start, end;
	public String ip;
	
	public Session(String address,long time){
		ip = address;
		start = time;
	}
	public void end(long time){
		end = time;
	}
	
	public long getStart(){
		return start;
	}
	public long getEnd(){
		return end;
	}
	public long getLength(){
		return end-start;
	}
	
	public String toString(){
		return "[session:"+ip+":" + start + "," + end + "]";
	}
	
	public static Session fromString(String data){
		if(!data.startsWith("[session:")){
			return null;
		}
		String[] vals = data.split(":");
		String[] times = vals[2].split(",");
		Session ret = new Session(vals[1], Long.parseLong(times[0]));
		ret.end( Long.parseLong(times[1].replace("]","")));
		return ret;
	}
	
}
