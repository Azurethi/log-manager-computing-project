package me.Azurethi.logManager;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

@SuppressWarnings("serial")
public class GUI extends javax.swing.JFrame {


	JTable tableArea;
	JTextPane ConsoleOutput;
	JTextField ConsoleInput;

	public GUI(){
		super("Log Manager v0.2");
		setSize(1000,500);
		setDefaultLookAndFeelDecorated(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		tableArea = new JTable(new Object[][]{{""}}, new String[]{"No data"});
		ConsoleOutput = new JTextPane();
		ConsoleInput = new JTextField(34);
		
		tableArea.setRowMargin(0);
		tableArea.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		ConsoleOutput.setEditable(false);
		ConsoleInput.addActionListener(new AbstractAction(){

			@Override
			public void actionPerformed(ActionEvent e) {
				Main.parseCommand(e.getActionCommand());
				ConsoleInput.setText("");
			}
			
		});
		JScrollPane tableScroll = new JScrollPane(tableArea,		JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		JScrollPane ConsoleScroll = new JScrollPane(ConsoleOutput,	JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		tableScroll.setPreferredSize(new Dimension(560,460));
		tableScroll.setBorder(BorderFactory.createLineBorder(Color.black));
		ConsoleScroll.setPreferredSize(new Dimension(380,420));
		ConsoleScroll.setBorder(BorderFactory.createLineBorder(Color.black));
		
		
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 2;
		gbc.ipadx = 10;
		add(tableScroll, gbc);
		gbc.gridx = 1;
		gbc.gridheight = 1;
		add(ConsoleScroll, gbc);
		gbc.gridy=1;
		add(ConsoleInput, gbc);
		
		setVisible(true);
	}
	
	
	public void println(String ln){println(ln, Color.black);}
	public void println(String ln, Color c){
		StyledDocument doc = ConsoleOutput.getStyledDocument();

        Style style = ConsoleOutput.addStyle("z", null);
        StyleConstants.setForeground(style, c);
        
        try {
        	doc.insertString(doc.getLength(), ln + "\n",style); 
        } catch (Exception ignored){}
        
        ConsoleOutput.setCaretPosition(ConsoleOutput.getDocument().getLength());
        
		//ConsoleOutput.setText(ConsoleOutput.getText() + ln + "\n");
	}

	public void updateTable(String[] head, String[][] data) {
		DefaultTableModel tableData = new DefaultTableModel(data,head);
		tableArea.setModel(tableData);
	}
	
}
