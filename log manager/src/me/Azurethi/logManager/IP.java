
package me.Azurethi.logManager;

import java.util.ArrayList;

public class IP {

	public String address;
	public ArrayList<String> uuids = new ArrayList<String>();
	public ArrayList<Long> pings = new ArrayList<Long>();
	
	public IP(String addr){
		address = addr;
	}
	public void addUUID(String uuid){
		if(!uuids.contains(uuid)) uuids.add(uuid);
	}
	public void addPing(Long time){
		pings.add(time);
	}
	public String toString() {
		String ret = "[ip:" + address + ":";
		for(String uuid : uuids){
			ret += uuid + ",";
		}
		ret = ret.substring(0, ret.length() - 1) + ":";
		for(Long ping : pings){
			ret += ping + ",";
		}
		return ret.substring(0, ret.length() - 1
				) + "]";
	}
	
	public static IP fromString(String data){
		if(!data.startsWith("[ip:")){
			return null;
		}
		String[] vals = data.split(":");
		IP ret = new IP(vals[1]);
		for(String uuid : vals[2].split(",")){
			ret.addUUID(uuid);
		}
		for(String ping : vals[3].split(",")){
			ret.addPing(Long.parseLong(ping.replace("]","")));
		}
		return ret;
	}
	

}
